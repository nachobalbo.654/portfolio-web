# My first web site
### Portfolio

This is the repository of my first web page, my portfolio.

The design concept is a home page talking about what am I currently working on. Then a bit about my skills and knowledge.

Above is a navigation menu:

<nav>
  <a href=#>Home</a> 
  <a href=#>Portfolio</a>  
  <a href=#>About me</a> 
</nav>

<p><p>

In the Portfolio page, all projects will be displayed and with a github link to everything.

About me section talks about my experiencies and some personal information.

Files included:
* HTML
    * index
* CSS
    * main
* JS
    * script


