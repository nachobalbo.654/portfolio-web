$(document).ready(function(){
    // Hide all tab content except the first one
    $('.tab-content').not('.tab-1').hide();

    // Tab click event handler
    $('.primary-nav a').click(function(e){
        e.preventDefault();

        // Hide all tab content
        $('.tab-content').hide();

        // Show the corresponding tab content
        var target = $(this).data('target');
        $(target).show();
    });
});